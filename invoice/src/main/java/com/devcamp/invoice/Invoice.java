package com.devcamp.invoice;

public class Invoice {
    int id;
    Customer customer;
    double amount;


    public Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public Customer getCustomer() {
        return customer;
    }


    public void setCustomer(Customer customer) {
        this.customer = customer;
    }


    public double getAmount() {
        return amount;
    }


    public void setAmount(double amount) {
        this.amount = amount;
    }


    public int getCustomerId(){
        return this.customer.id;
    }
    

    public String getCustomerName(){
        return this.customer.name;
    }

    public int getCustomerDiscount(){
        return this.customer.discount;
    }

    public double getAmountAfterDiscount(){
        return amount - (amount * getCustomerDiscount()/100);
    }


 @Override
    public String toString() {
        return String.format("Invoices[id=%s, customer=%s(discount %s%%), amount=%s]",this.id,this.customer.name,this.customer.discount,this.amount);
    }
}